/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaolei.lib.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 * @author Gao Lei
 */
public class FileWriteUtils {

	public static void write(String filePath, String text, boolean append)
			throws IOException {
		FileWriter fstream = new FileWriter(filePath, append);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write(text);
		out.flush();
		out.close();
	}

	public static void append(String filePath, String text) throws IOException {
		write(filePath, text, true);
	}

	public static void appendtoNewLine(String filePath, String text)
			throws IOException {
		FileWriter fstream = new FileWriter(filePath, true);
		BufferedWriter out = new BufferedWriter(fstream);
		out.write("\n" + text);
		out.flush();
		out.close();
	}

	public static void rewrite(String filePath, String text) throws IOException {
		write(filePath, text, false);
	}
}
