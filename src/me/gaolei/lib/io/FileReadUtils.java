/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.gaolei.lib.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author glei
 */
public class FileReadUtils {

    public static String read(String filePath) throws IOException {
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);
        String strLine = "";
        StringBuffer allLine = new StringBuffer();
        while ((strLine = br.readLine()) != null) {
            allLine.append(strLine + "\n");

        }
        return allLine.toString();
    }

    public static String readtoOneLine(String filePath) throws IOException {
        FileReader fr = new FileReader(filePath);
        BufferedReader br = new BufferedReader(fr);
        String strLine = "";
        StringBuffer allLine = new StringBuffer();
        while ((strLine = br.readLine()) != null) {
            allLine.append(strLine);
        }
        return allLine.toString();
    }
}
