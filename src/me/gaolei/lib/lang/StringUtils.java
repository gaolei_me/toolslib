package me.gaolei.lib.lang;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author Gao Lei
 */
public class StringUtils {
	
	
	public static String getSha1(String str, String chartset) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {
		byte[] bytes = str.getBytes(chartset);
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		bytes = md.digest(bytes);
		Formatter formatter = new Formatter();
		for (byte b : bytes) {
			formatter.format("%02x", b);
		}

		return formatter.toString();
	}

	public static String getMD5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1, messageDigest);
			String hashtext = number.toString(16);
			// Now we need to zero pad it if you actually want the full 32
			// chars.
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Returns a new string resulting from replacing the last char
	 * 
	 * @param str
	 * @return
	 */
	public static String deleteLastChar(String str) {
		return str.substring(0, str.length() - 1);
	}

	public static List<String> getAll(String str, String beginStr, String endStr) {
		List<String> list = new ArrayList<String>();
		Pattern p = Pattern.compile("<id>.+?>");
		Matcher m = p.matcher(str);
		while (m.find()) {
			list.add(str.substring(m.start(), m.end()));
		}
		return list;
	}

	/**
	 * The function is to delete all string that start with beginStr and end
	 * with endStr
	 * 
	 * @param str
	 *            the old string
	 * @param beginStr
	 *            beginning string
	 * 
	 * @param endStr
	 *            end string
	 * 
	 * @return the new string
	 * 
	 */

	public static String deleteAll(String str, String beginStr, String endStr) {

		return replaceAll(str, beginStr, endStr, "");
	}

	/**
	 * The function is to replace all string that start with beginStr and end
	 * with endStr
	 * 
	 * @param str
	 *            the old string
	 * @param beginStr
	 *            beginning string
	 * 
	 * @param endStr
	 *            end string
	 * @param replaceStr
	 *            the replaced string
	 * 
	 * @return the new string
	 * 
	 */
	public static String replaceAll(String str, String beginStr, String endStr, String replaceStr) {
		String regex = beginStr + "([\\d\\D]*?)" + endStr;

		str = str.replaceAll(regex, replaceStr);
		return str;
	}

	/**
	 * The function is to replace the first string from fromIndex
	 * 
	 * 
	 * 
	 * @param str
	 *            the old string
	 * 
	 * @param regex
	 *            the regular expression to which this string is to be matched
	 * 
	 * @param fromIndex
	 *            the start Index
	 * @param replacement
	 *            the string to be substituted for the first match
	 * @return the new string
	 * 
	 */
	public static String replace(String str, String regex, String replacement, int fromIndex) {
		String front = str.substring(0, fromIndex);
		String rear = str.substring(fromIndex);
		rear = rear.replaceFirst(regex, replacement);
		return front + rear;

	}

	public static String deleteCN(String str) {
		StringBuffer ret = new StringBuffer();
		char[] charArray = str.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			if ((charArray[i] >= 0x4e00) && (charArray[i] <= 0x9fbb)) {
				// System.out.println(charArray[i]);
			} else {
				ret.append(charArray[i]);
			}
		}
		return ret.toString();
	}

	public boolean validEmail(String email) {

		// Set the email pattern string
		Pattern p = Pattern
				.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		// Match the given string with the pattern
		Matcher m = p.matcher(email);

		// check whether match is found
		return m.matches();

	}
}
