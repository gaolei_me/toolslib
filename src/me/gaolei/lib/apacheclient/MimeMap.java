package me.gaolei.lib.apacheclient;

import java.util.Enumeration;
import java.util.Hashtable;

public class MimeMap {
	private static Hashtable<String, String> extensionContentTypeMap = new Hashtable<String, String>(101);
	static {
		extensionContentTypeMap.put("txt", "text/plain");
		extensionContentTypeMap.put("html", "text/html");
		extensionContentTypeMap.put("htm", "text/html");
		extensionContentTypeMap.put("gif", "image/gif");
		extensionContentTypeMap.put("jpg", "image/jpeg");
		extensionContentTypeMap.put("jpe", "image/jpeg");
		extensionContentTypeMap.put("jpeg", "image/jpeg");
		extensionContentTypeMap.put("png", "image/png");
		extensionContentTypeMap.put("bmp", "image/bmp");
		extensionContentTypeMap.put("java", "text/plain");
		extensionContentTypeMap.put("body", "text/html");
		extensionContentTypeMap.put("rtx", "text/richtext");
		extensionContentTypeMap.put("tsv", "text/tab-separated-values");
		extensionContentTypeMap.put("etx", "text/x-setext");
		extensionContentTypeMap.put("ps", "application/x-postscript");
		extensionContentTypeMap.put("class", "application/java");
		extensionContentTypeMap.put("csh", "application/x-csh");
		extensionContentTypeMap.put("sh", "application/x-sh");
		extensionContentTypeMap.put("tcl", "application/x-tcl");
		extensionContentTypeMap.put("tex", "application/x-tex");
		extensionContentTypeMap.put("texinfo", "application/x-texinfo");
		extensionContentTypeMap.put("texi", "application/x-texinfo");
		extensionContentTypeMap.put("t", "application/x-troff");
		extensionContentTypeMap.put("tr", "application/x-troff");
		extensionContentTypeMap.put("roff", "application/x-troff");
		extensionContentTypeMap.put("man", "application/x-troff-man");
		extensionContentTypeMap.put("me", "application/x-troff-me");
		extensionContentTypeMap.put("ms", "application/x-wais-source");
		extensionContentTypeMap.put("src", "application/x-wais-source");
		extensionContentTypeMap.put("zip", "application/zip");
		extensionContentTypeMap.put("bcpio", "application/x-bcpio");
		extensionContentTypeMap.put("cpio", "application/x-cpio");
		extensionContentTypeMap.put("gtar", "application/x-gtar");
		extensionContentTypeMap.put("shar", "application/x-shar");
		extensionContentTypeMap.put("sv4cpio", "application/x-sv4cpio");
		extensionContentTypeMap.put("sv4crc", "application/x-sv4crc");
		extensionContentTypeMap.put("tar", "application/x-tar");
		extensionContentTypeMap.put("ustar", "application/x-ustar");
		extensionContentTypeMap.put("dvi", "application/x-dvi");
		extensionContentTypeMap.put("hdf", "application/x-hdf");
		extensionContentTypeMap.put("latex", "application/x-latex");
		extensionContentTypeMap.put("bin", "application/octet-stream");
		extensionContentTypeMap.put("oda", "application/oda");
		extensionContentTypeMap.put("pdf", "application/pdf");
		extensionContentTypeMap.put("ps", "application/postscript");
		extensionContentTypeMap.put("eps", "application/postscript");
		extensionContentTypeMap.put("ai", "application/postscript");
		extensionContentTypeMap.put("rtf", "application/rtf");
		extensionContentTypeMap.put("nc", "application/x-netcdf");
		extensionContentTypeMap.put("cdf", "application/x-netcdf");
		extensionContentTypeMap.put("cer", "application/x-x509-ca-cert");
		extensionContentTypeMap.put("exe", "application/octet-stream");
		extensionContentTypeMap.put("gz", "application/x-gzip");
		extensionContentTypeMap.put("Z", "application/x-compress");
		extensionContentTypeMap.put("z", "application/x-compress");
		extensionContentTypeMap.put("hqx", "application/mac-binhex40");
		extensionContentTypeMap.put("mif", "application/x-mif");
		extensionContentTypeMap.put("ief", "image/ief");
		extensionContentTypeMap.put("tiff", "image/tiff");
		extensionContentTypeMap.put("tif", "image/tiff");
		extensionContentTypeMap.put("ras", "image/x-cmu-raster");
		extensionContentTypeMap.put("pnm", "image/x-portable-anymap");
		extensionContentTypeMap.put("pbm", "image/x-portable-bitmap");
		extensionContentTypeMap.put("pgm", "image/x-portable-graymap");
		extensionContentTypeMap.put("ppm", "image/x-portable-pixmap");
		extensionContentTypeMap.put("rgb", "image/x-rgb");
		extensionContentTypeMap.put("xbm", "image/x-xbitmap");
		extensionContentTypeMap.put("xpm", "image/x-xpixmap");
		extensionContentTypeMap.put("xwd", "image/x-xwindowdump");
		extensionContentTypeMap.put("au", "audio/basic");
		extensionContentTypeMap.put("snd", "audio/basic");
		extensionContentTypeMap.put("aif", "audio/x-aiff");
		extensionContentTypeMap.put("aiff", "audio/x-aiff");
		extensionContentTypeMap.put("aifc", "audio/x-aiff");
		extensionContentTypeMap.put("wav", "audio/x-wav");
		extensionContentTypeMap.put("mpeg", "video/mpeg");
		extensionContentTypeMap.put("mpg", "video/mpeg");
		extensionContentTypeMap.put("mpe", "video/mpeg");
		extensionContentTypeMap.put("qt", "video/quicktime");
		extensionContentTypeMap.put("mov", "video/quicktime");
		extensionContentTypeMap.put("avi", "video/x-msvideo");
		extensionContentTypeMap.put("movie", "video/x-sgi-movie");
		extensionContentTypeMap.put("avx", "video/x-rad-screenplay");
		extensionContentTypeMap.put("wrl", "x-world/x-vrml");
		extensionContentTypeMap.put("mpv2", "video/mpeg2");

		/* Add XML related MIMEs */

		extensionContentTypeMap.put("xml", "text/xml");
		extensionContentTypeMap.put("xsl", "text/xml");
		extensionContentTypeMap.put("svg", "image/svg+xml");
		extensionContentTypeMap.put("svgz", "image/svg+xml");
		extensionContentTypeMap.put("wbmp", "image/vnd.wap.wbmp");
		extensionContentTypeMap.put("wml", "text/vnd.wap.wml");
		extensionContentTypeMap.put("wmlc", "application/vnd.wap.wmlc");
		extensionContentTypeMap.put("wmls", "text/vnd.wap.wmlscript");
		extensionContentTypeMap.put("wmlscriptc", "application/vnd.wap.wmlscriptc");
	}

	private static Hashtable<String, String> contentTypeExtensionMap = new Hashtable<String, String>(101);
	static {
		contentTypeExtensionMap.put("text/plain", "txt");
		contentTypeExtensionMap.put("text/html", "html");
		contentTypeExtensionMap.put("image/gif", "gif");
		contentTypeExtensionMap.put("image/jpeg", "jpg");
		contentTypeExtensionMap.put("image/png", "png");
		contentTypeExtensionMap.put("image/bmp", "bmp");

	}

	public static boolean isValidExtension(String extension) {
		return extensionContentTypeMap.containsKey(extension);
	}

	public static String getExtentsion(String contentType) {
		return MimeMap.contentTypeExtensionMap.get(contentType);
	}
}
