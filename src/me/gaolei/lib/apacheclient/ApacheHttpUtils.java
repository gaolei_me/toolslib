package me.gaolei.lib.apacheclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class ApacheHttpUtils {
	public static final String CONTENT_TYPE = "Content-Type";

	public static String post(String url, Map<String, String> parameters) throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		for (Entry<String, String> parameter : parameters.entrySet()) {
			nameValuePairs.add(new BasicNameValuePair(parameter.getKey(), parameter.getValue()));
		}
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer allLine = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			allLine.append(line + "\n");
		}
		return allLine.toString();
	}

	public static String get(String url) throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);
		System.out.println("executing request" + httpget.getRequestLine());
		HttpResponse response = httpclient.execute(httpget);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer allLine = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			allLine.append(line + "\n");
		}
		return allLine.toString();
	}

	public static String get(String url, Map<String, String> hearderMap) throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);
		for (Entry<String, String> hearder : hearderMap.entrySet()) {

			httpget.setHeader(hearder.getKey(), hearder.getValue());
		}

		System.out.println("executing request" + httpget.getRequestLine());
		HttpResponse response = httpclient.execute(httpget);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer allLine = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			allLine.append(line + "\n");
		}
		return allLine.toString();
	}

	public static String post(String url, Map<String, String> hearderMap, Map<String, String> parameterMap)
			throws ClientProtocolException, IOException {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		// set parameters
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		for (Entry<String, String> parameter : parameterMap.entrySet()) {
			nameValuePairs.add(new BasicNameValuePair(parameter.getKey(), parameter.getValue()));
		}
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		// set header
		for (Entry<String, String> hearder : hearderMap.entrySet()) {
			post.setHeader(hearder.getKey(), hearder.getValue());
		}

		// get response
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer allLine = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			allLine.append(line + "\n");
		}
		return allLine.toString();
	}

	/**
	 * Get extension of url
	 * 
	 * @param photoURL
	 * @param extensionList
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String getURLExtension(String photoURL) throws ClientProtocolException, IOException {
		String extension = null;
		int index = photoURL.lastIndexOf(".") + 1;
		if (index > 0) {
			extension = photoURL.substring(index);
		}
		if (index <= 0 || !MimeMap.isValidExtension(extension)) {
			String contentType = getResponseHeader(photoURL, CONTENT_TYPE);
			extension = MimeMap.getExtentsion(contentType);
		}

		return extension;
	}

	/**
	 * Get response header value
	 * 
	 * @param url
	 * @param headerName
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String getResponseHeader(String url, String headerName) throws ClientProtocolException, IOException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(encodeURL(url));
		HttpResponse response = httpclient.execute(httpget);
		Header[] headers = response.getHeaders(headerName);
		if (headers.length < 1) {
			return null;
		} else {
			return headers[0].getValue();
		}
	}

	/**
	 * check valid
	 * 
	 * @param url
	 * @return
	 */
	public static boolean isValidURL(String url) {
		try {
			new HttpGet(url);
		} catch (java.lang.IllegalArgumentException e) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param url
	 * @return
	 * @throws URIException
	 *             if the default protocol charset is not supported
	 */
	public static String encodeURL(String url) throws URIException {
		if (isValidURL(url)) {
			return url;
		} else {
			return URIUtil.encodeQuery(url);
		}
	}

}
